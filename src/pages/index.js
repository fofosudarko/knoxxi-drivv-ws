import { useEffect, useRef, useCallback, useState, useReducer } from 'react';
import { useMediaQuery } from 'react-responsive';
import * as Stomp from '@stomp/stompjs';
import create from 'zustand';
import { Container, Row, Col, Button, Stack, Accordion } from 'react-bootstrap';
import { useReactToPrint } from 'react-to-print';

const KNOXXI_DRIVV_MESSAGES_WS_PATH =
  process.env.NEXT_PUBLIC_KNOXXI_DRIVV_MESSAGES_WS_PATH;
const KNOXXI_DRIVV_WS_URL = process.env.NEXT_PUBLIC_KNOXXI_DRIVV_WS_URL;
const LOCATION_INFO_QUEUE_DESTINATION = '/queue/location.info';
const DIAGNOSTIC_INFO_QUEUE_DESTINATION = '/queue/diagnostic.info';
const TYRE_PRESSURE_INFO_QUEUE_DESTINATION = '/queue/tyre.pressure.info';
const AUTHORIZATION = process.env.NEXT_PUBLIC_AUTHENTICATION_BEARER_TOKEN;

function reducer(state, action) {
  switch (action.type) {
    case 'UPDATE_DEVICE_DIMENSIONS':
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

function useDeviceDimensions() {
  const initialState = {
    isSmallDevice: false,
    isMediumDevice: false,
    isLargeDevice: false,
  };
  const [state, dispatch] = useReducer(reducer, initialState);

  const isSmallDevice = useMediaQuery({ maxWidth: 576 });
  const isMediumDevice = useMediaQuery({ maxWidth: 768 });
  const isLargeDevice = useMediaQuery({ minWidth: 992 });

  useEffect(() => {
    dispatch({
      type: 'UPDATE_DEVICE_DIMENSIONS',
      payload: { isSmallDevice, isMediumDevice, isLargeDevice },
    });
  }, [isSmallDevice, isMediumDevice, isLargeDevice]);

  return {
    isSmallDevice: state.isSmallDevice,
    isMediumDevice: state.isMediumDevice,
    isLargeDevice: state.isLargeDevice,
  };
}

const useStore = create((set) => ({
  locationInfoMessages: [],
  diagnosticInfoMessages: [],
  tyrePressureInfoMessages: [],
  addLocationInfoMessage: (locationInfoMessage) =>
    set((state) => ({
      locationInfoMessages: [
        locationInfoMessage,
        ...state.locationInfoMessages,
      ],
    })),
  addDiagnosticInfoMessage: (diagnosticInfoMessage) =>
    set((state) => ({
      diagnosticInfoMessages: [
        diagnosticInfoMessage,
        ...state.diagnosticInfoMessages,
      ],
    })),
  addTyrePressureInfoMessage: (tyrePressureInfoMessage) =>
    set((state) => ({
      tyrePressureInfoMessages: [
        tyrePressureInfoMessage,
        ...state.tyrePressureInfoMessages,
      ],
    })),
  clearLocationInfoMessages: () => set((_) => ({ locationInfoMessages: [] })),
  clearDiagnosticInfoMessages: () =>
    set((_) => ({ diagnosticInfoMessages: [] })),
  clearTyrePressureInfoMessages: () =>
    set((_) => ({ tyrePressureInfoMessages: [] })),
}));

function useInitializeStompClient({ type = 'General', url, isDebug = false }) {
  const [connecting, setConnecting] = useState(false);
  const [disconnecting, setDisconnecting] = useState(false);
  const [stompClient, setStompClient] = useState(null);
  const stompClientRef = useRef(null);
  const handleStompConnect = useCallback(
    (frame) => {
      setConnecting(false);
      console.log(`${type} stomp client connected`);
      setStompClient(stompClientRef.current);
    },
    [type]
  );
  const handleStompError = useCallback(
    (frame) => {
      setDisconnecting(false);
      console.log(`${type} stomp client error occurred`);
      setStompClient(null);
    },
    [type]
  );
  const handleInitializeStompClient = useCallback(() => {
    try {
      setConnecting(true);
      console.log(`Initializing ${type.toLocaleLowerCase()} stomp client...`);
      stompClientRef.current = new Stomp.Client({
        brokerURL: undefined,
        debug: isDebug
          ? (message) => {
              console.log('Debug: ', message);
            }
          : () => {},
        connectHeaders: {
          authorization: AUTHORIZATION,
        },
        discardWebsocketOnCommFailure: true,
      });
      stompClientRef.current.webSocketFactory = () => {
        return new window.WebSocket(url);
      };
      stompClientRef.current.onConnect = handleStompConnect;
      stompClientRef.current.onStompError = handleStompError;
      stompClientRef.current.activate();
    } catch (error) {
      setConnecting(false);
      console.error(error);
    }
  }, [handleStompConnect, handleStompError, isDebug, type, url]);
  const handleStompDisconnect = useCallback(async () => {
    setConnecting(false);
    setDisconnecting(true);
    console.log(`${type} stomp client disconnected`);
    await stompClientRef.current?.forceDisconnect();
    await stompClientRef.current?.deactivate();
    setStompClient(null);
    stompClientRef.current = null;
    setDisconnecting(false);
  }, [type]);
  return {
    stompClient,
    setStompClient,
    handleInitializeStompClient,
    handleStompDisconnect,
    connecting,
    setConnecting,
    disconnecting,
    setDisconnecting,
  };
}

function useListLocationInfoMessages({ stompClient }) {
  const {
    locationInfoMessages,
    addLocationInfoMessage,
    clearLocationInfoMessages,
  } = useStore((state) => {
    const {
      locationInfoMessages,
      addLocationInfoMessage,
      clearLocationInfoMessages,
    } = state;
    return {
      locationInfoMessages,
      addLocationInfoMessage,
      clearLocationInfoMessages,
    };
  });
  const [hasSubscribed, setHasSubscribed] = useState(false);

  const subscriptionRef = useRef(null);

  const handleLocationInfoMessagesSubscribe = useCallback(() => {
    if (stompClient && stompClient.active && stompClient.connected) {
      subscriptionRef.current = stompClient.subscribe(
        LOCATION_INFO_QUEUE_DESTINATION,
        (message) => {
          addLocationInfoMessage(JSON.parse(message.body));
        }
      );
      setHasSubscribed(true);
    }
  }, [addLocationInfoMessage, stompClient]);

  const handleLocationInfoMessagesUnsubscribe = useCallback(() => {
    subscriptionRef.current?.unsubscribe();
    clearLocationInfoMessages();
    subscriptionRef.current = null;
    setHasSubscribed(false);
  }, [clearLocationInfoMessages]);

  return {
    locationInfoMessages,
    clearLocationInfoMessages,
    subscriptionRef,
    handleLocationInfoMessagesSubscribe,
    handleLocationInfoMessagesUnsubscribe,
    hasSubscribed,
    setHasSubscribed,
  };
}

function useListDiagnosticInfoMessages({ stompClient }) {
  const {
    diagnosticInfoMessages,
    addDiagnosticInfoMessage,
    clearDiagnosticInfoMessages,
  } = useStore((state) => {
    const {
      diagnosticInfoMessages,
      addDiagnosticInfoMessage,
      clearDiagnosticInfoMessages,
    } = state;
    return {
      diagnosticInfoMessages,
      addDiagnosticInfoMessage,
      clearDiagnosticInfoMessages,
    };
  });
  const [hasSubscribed, setHasSubscribed] = useState(false);

  const subscriptionRef = useRef(null);

  const handleDiagnosticInfoMessagesSubscribe = useCallback(() => {
    if (stompClient && stompClient.active && stompClient.connected) {
      subscriptionRef.current = stompClient.subscribe(
        DIAGNOSTIC_INFO_QUEUE_DESTINATION,
        (message) => {
          addDiagnosticInfoMessage(JSON.parse(message.body));
        }
      );
      setHasSubscribed(true);
    }
  }, [addDiagnosticInfoMessage, stompClient]);

  const handleDiagnosticInfoMessagesUnsubscribe = useCallback(() => {
    subscriptionRef.current?.unsubscribe();
    clearDiagnosticInfoMessages();
    subscriptionRef.current = null;
    setHasSubscribed(false);
  }, [clearDiagnosticInfoMessages]);

  return {
    diagnosticInfoMessages,
    clearDiagnosticInfoMessages,
    subscriptionRef,
    handleDiagnosticInfoMessagesSubscribe,
    handleDiagnosticInfoMessagesUnsubscribe,
    hasSubscribed,
    setHasSubscribed,
  };
}

function useListTyrePressureInfoMessages({ stompClient }) {
  const {
    tyrePressureInfoMessages,
    addTyrePressureInfoMessage,
    clearTyrePressureInfoMessages,
  } = useStore((state) => {
    const {
      tyrePressureInfoMessages,
      addTyrePressureInfoMessage,
      clearTyrePressureInfoMessages,
    } = state;
    return {
      tyrePressureInfoMessages,
      addTyrePressureInfoMessage,
      clearTyrePressureInfoMessages,
    };
  });
  const [hasSubscribed, setHasSubscribed] = useState(false);

  const subscriptionRef = useRef(null);

  const handleTyrePressureInfoMessagesSubscribe = useCallback(() => {
    if (stompClient && stompClient.active && stompClient.connected) {
      subscriptionRef.current = stompClient.subscribe(
        TYRE_PRESSURE_INFO_QUEUE_DESTINATION,
        (message) => {
          addTyrePressureInfoMessage(JSON.parse(message.body));
        }
      );
      setHasSubscribed(true);
    }
  }, [addTyrePressureInfoMessage, stompClient]);

  const handleTyrePressureInfoMessagesUnsubscribe = useCallback(() => {
    subscriptionRef.current?.unsubscribe();
    clearTyrePressureInfoMessages();
    subscriptionRef.current = null;
    setHasSubscribed(false);
  }, [clearTyrePressureInfoMessages]);

  return {
    tyrePressureInfoMessages,
    clearTyrePressureInfoMessages,
    subscriptionRef,
    handleTyrePressureInfoMessagesSubscribe,
    handleTyrePressureInfoMessagesUnsubscribe,
    hasSubscribed,
    setHasSubscribed,
  };
}

export default function Home() {
  return <MessagesContainer />;
}

function MessagesContainer() {
  const { isLargeDevice } = useDeviceDimensions();
  const {
    stompClient,
    handleInitializeStompClient,
    handleStompDisconnect: _handleStompDisconnect,
    connecting,
    disconnecting,
  } = useInitializeStompClient({
    url: KNOXXI_DRIVV_WS_URL + KNOXXI_DRIVV_MESSAGES_WS_PATH,
    isDebug: true,
    type: 'Messages',
  });

  const {
    locationInfoMessages,
    handleLocationInfoMessagesSubscribe,
    handleLocationInfoMessagesUnsubscribe,
    clearLocationInfoMessages,
    subscriptionRef: locationInfoMessagesSubscriptionRef,
    hasSubscribed: hasSubscribedToLocationInfoMessages,
    setHasSubscribed: setHasSubscribedToLocationInfoMessages,
  } = useListLocationInfoMessages({
    stompClient,
  });

  const {
    diagnosticInfoMessages,
    handleDiagnosticInfoMessagesSubscribe,
    handleDiagnosticInfoMessagesUnsubscribe,
    clearDiagnosticInfoMessages,
    subscriptionRef: diagnosticInfoMessagesSubscriptionRef,
    hasSubscribed: hasSubscribedToDiagnosticInfoMessages,
    setHasSubscribed: setHasSubscribedToDiagnosticInfoMessages,
  } = useListDiagnosticInfoMessages({
    stompClient,
  });

  const {
    tyrePressureInfoMessages,
    handleTyrePressureInfoMessagesSubscribe,
    handleTyrePressureInfoMessagesUnsubscribe,
    clearTyrePressureInfoMessages,
    subscriptionRef: tyrePressureInfoMessagesSubscriptionRef,
    hasSubscribed: hasSubscribedToTyrePressureInfoMessages,
    setHasSubscribed: setHasSubscribedToTyrePressureInfoMessages,
  } = useListTyrePressureInfoMessages({
    stompClient,
  });

  const handleStompDisconnect = useCallback(() => {
    locationInfoMessagesSubscriptionRef.current?.unsubscribe();
    diagnosticInfoMessagesSubscriptionRef.current?.unsubscribe();
    tyrePressureInfoMessagesSubscriptionRef.current?.unsubscribe();
    _handleStompDisconnect();
    clearLocationInfoMessages();
    clearDiagnosticInfoMessages();
    clearTyrePressureInfoMessages();
    locationInfoMessagesSubscriptionRef.current = null;
    diagnosticInfoMessagesSubscriptionRef.current = null;
    tyrePressureInfoMessagesSubscriptionRef.current = null;
    setHasSubscribedToLocationInfoMessages(false);
    setHasSubscribedToDiagnosticInfoMessages(false);
    setHasSubscribedToTyrePressureInfoMessages(false);
  }, [
    _handleStompDisconnect,
    clearDiagnosticInfoMessages,
    clearLocationInfoMessages,
    clearTyrePressureInfoMessages,
    diagnosticInfoMessagesSubscriptionRef,
    locationInfoMessagesSubscriptionRef,
    setHasSubscribedToDiagnosticInfoMessages,
    setHasSubscribedToLocationInfoMessages,
    setHasSubscribedToTyrePressureInfoMessages,
    tyrePressureInfoMessagesSubscriptionRef,
  ]);

  return (
    <Container fluid className="mt-2">
      <Stack direction="horizontal" gap={2} className="mb-2">
        {stompClient ? (
          <Button onClick={handleStompDisconnect}>
            {disconnecting ? 'Disconnecting...' : 'Disconnect'}
          </Button>
        ) : (
          <Button
            onClick={!connecting ? handleInitializeStompClient : undefined}
            disabled={connecting}
          >
            {connecting ? 'Connecting...' : 'Connect'}
          </Button>
        )}
        {connecting ? (
          <Button
            onClick={!disconnecting ? handleStompDisconnect : undefined}
            disabled={disconnecting}
          >
            {disconnecting ? 'Disconnecting...' : 'Disconnect'}
          </Button>
        ) : null}
      </Stack>
      <div>
        Connection status: <b>{stompClient ? 'Connected' : 'Disconnected'}</b>
      </div>
      <br />
      <div className="overflow-auto">
        {isLargeDevice ? (
          <Row>
            <Col>
              <LocationInfoMessagesContainer
                onSubscribe={handleLocationInfoMessagesSubscribe}
                onUnsubscribe={handleLocationInfoMessagesUnsubscribe}
                onClear={clearLocationInfoMessages}
                locationInfoMessages={locationInfoMessages}
                hasSubscribed={hasSubscribedToLocationInfoMessages}
              />
            </Col>
            <Col>
              <DiagnosticInfoMessagesContainer
                onSubscribe={handleDiagnosticInfoMessagesSubscribe}
                onUnsubscribe={handleDiagnosticInfoMessagesUnsubscribe}
                onClear={clearDiagnosticInfoMessages}
                diagnosticInfoMessages={diagnosticInfoMessages}
                hasSubscribed={hasSubscribedToDiagnosticInfoMessages}
              />
            </Col>
            <Col>
              <TyrePressureInfoMessagesContainer
                onSubscribe={handleTyrePressureInfoMessagesSubscribe}
                onUnsubscribe={handleTyrePressureInfoMessagesUnsubscribe}
                onClear={clearTyrePressureInfoMessages}
                tyrePressureInfoMessages={tyrePressureInfoMessages}
                hasSubscribed={hasSubscribedToTyrePressureInfoMessages}
              />
            </Col>
          </Row>
        ) : (
          <Accordion defaultActiveKey="location-info-messages">
            <Accordion.Item eventKey="location-info-messages">
              <Accordion.Header>Location info messages</Accordion.Header>
              <Accordion.Body>
                <LocationInfoMessagesContainer
                  onSubscribe={handleLocationInfoMessagesSubscribe}
                  onUnsubscribe={handleLocationInfoMessagesUnsubscribe}
                  onClear={clearLocationInfoMessages}
                  locationInfoMessages={locationInfoMessages}
                  hasSubscribed={hasSubscribedToLocationInfoMessages}
                />
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="diagnostic-info-messages">
              <Accordion.Header>Diagnostic info messages</Accordion.Header>
              <Accordion.Body>
                <DiagnosticInfoMessagesContainer
                  onSubscribe={handleDiagnosticInfoMessagesSubscribe}
                  onUnsubscribe={handleDiagnosticInfoMessagesUnsubscribe}
                  onClear={clearDiagnosticInfoMessages}
                  diagnosticInfoMessages={diagnosticInfoMessages}
                  hasSubscribed={hasSubscribedToDiagnosticInfoMessages}
                />
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="tyre-pressure-info-messages">
              <Accordion.Header>Tyre pressure info messages</Accordion.Header>
              <Accordion.Body>
                <TyrePressureInfoMessagesContainer
                  onSubscribe={handleTyrePressureInfoMessagesSubscribe}
                  onUnsubscribe={handleTyrePressureInfoMessagesUnsubscribe}
                  onClear={clearTyrePressureInfoMessages}
                  tyrePressureInfoMessages={tyrePressureInfoMessages}
                  hasSubscribed={hasSubscribedToTyrePressureInfoMessages}
                />
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        )}
      </div>
    </Container>
  );
}

function LocationInfoMessagesContainer({
  onSubscribe,
  onUnsubscribe,
  onClear,
  locationInfoMessages,
  hasSubscribed,
}) {
  const printRef = useRef();
  const handlePrint = useReactToPrint({ content: () => printRef.current });
  return (
    <div>
      <div className="fw-bold fs-4 mb-2 text-nowrap w-100">
        Location info messages
      </div>
      <Stack direction="horizontal" gap={2} className="mb-2">
        {hasSubscribed ? (
          <Button onClick={onUnsubscribe}>Unsubscribe</Button>
        ) : (
          <Button onClick={onSubscribe}>Subscribe</Button>
        )}
        <Button onClick={onClear}>Clear</Button>
        <Button onClick={handlePrint}>Print</Button>
      </Stack>
      <div className="messages-list overflow-auto" ref={printRef}>
        {locationInfoMessages?.length ? (
          locationInfoMessages.map((item) => (
            <>
              <LocationInfoMessageItem
                key={item.id}
                locationInfoMessage={item}
              />
              <br />
            </>
          ))
        ) : (
          <span>No location info messages yet</span>
        )}
      </div>
    </div>
  );
}

function LocationInfoMessageItem({ locationInfoMessage }) {
  const { id, imei, createdOn, latitude, longitude, altitude } =
    locationInfoMessage ?? {};
  return (
    <div>
      <div>Id: {id}</div>
      <div>Imei: {imei}</div>
      <div>Created On: {createdOn}</div>
      <div>Latitude: {latitude}</div>
      <div>Longitude: {longitude}</div>
      <div>Altitude: {altitude}</div>
    </div>
  );
}

function DiagnosticInfoMessagesContainer({
  onSubscribe,
  onUnsubscribe,
  onClear,
  diagnosticInfoMessages,
  hasSubscribed,
}) {
  const printRef = useRef();
  const handlePrint = useReactToPrint({ content: () => printRef.current });
  return (
    <div>
      <div className="fw-bold fs-4 mb-2 text-nowrap w-100">
        Diagnostic info messages
      </div>
      <Stack direction="horizontal" gap={2} className="mb-2">
        {hasSubscribed ? (
          <Button onClick={onUnsubscribe}>Unsubscribe</Button>
        ) : (
          <Button onClick={onSubscribe}>Subscribe</Button>
        )}
        <Button onClick={onClear}>Clear</Button>
        <Button onClick={handlePrint}>Print</Button>
      </Stack>
      <div className="messages-list overflow-auto" ref={printRef}>
        {diagnosticInfoMessages?.length ? (
          diagnosticInfoMessages.map((item) => (
            <>
              <DiagnosticInfoMessageItem
                key={item.id}
                diagnosticInfoMessage={item}
              />
              <br />
            </>
          ))
        ) : (
          <span>No diagnostic info messages yet</span>
        )}
      </div>
    </div>
  );
}

function DiagnosticInfoMessageItem({ diagnosticInfoMessage }) {
  const {
    id,
    imei,
    createdOn,
    accumulativeMileage,
    instantFuel,
    averageFuel,
    drivingTime,
    speed,
    engineSpeed,
    powerLoad,
    waterTemperature,
  } = diagnosticInfoMessage ?? {};
  return (
    <div>
      <div>Id: {id}</div>
      <div>Imei: {imei}</div>
      <div>Created On: {createdOn}</div>
      <div>Accumulative mileage: {accumulativeMileage}</div>
      <div>Instant fuel: {instantFuel}</div>
      <div>Average fuel: {averageFuel}</div>
      <div>Driving time: {drivingTime}</div>
      <div>speed: {speed}</div>
      <div>Engine speed: {engineSpeed}</div>
      <div>Power load: {powerLoad}</div>
      <div>Water temperature: {waterTemperature}</div>
    </div>
  );
}

function TyrePressureInfoMessagesContainer({
  onSubscribe,
  onUnsubscribe,
  onClear,
  tyrePressureInfoMessages,
  hasSubscribed,
}) {
  const printRef = useRef();
  const handlePrint = useReactToPrint({ content: () => printRef.current });
  return (
    <div>
      <div className="fw-bold fs-4 mb-2 text-nowrap w-100">
        Tyre pressure info messages
      </div>
      <Stack direction="horizontal" gap={2} className="mb-2">
        {hasSubscribed ? (
          <Button onClick={onUnsubscribe}>Unsubscribe</Button>
        ) : (
          <Button onClick={onSubscribe}>Subscribe</Button>
        )}
        <Button onClick={onClear}>Clear</Button>
        <Button onClick={handlePrint}>Print</Button>
      </Stack>
      <div className="messages-list overflow-auto" ref={printRef}>
        {tyrePressureInfoMessages?.length ? (
          tyrePressureInfoMessages.map((item) => (
            <>
              <TyrePressureInfoMessageItem
                key={item.id}
                tyrePressureInfoMessage={item}
              />
              <br />
            </>
          ))
        ) : (
          <span>No tyre pressure info messages yet</span>
        )}
      </div>
    </div>
  );
}

function TyrePressureInfoMessageItem({ tyrePressureInfoMessage }) {
  const {
    id,
    imei,
    createdOn,
    leftFrontTyrePressure,
    leftFrontTyreTemperature,
    leftFrontTyreState,
    rightFrontTyrePressure,
    rightFrontTyreTemperature,
    rightFrontTyreState,
    leftRearTyrePressure,
    leftRearTyreTemperature,
    leftRearTyreState,
    rightRearTyrePressure,
    rightRearTyreTemperature,
    rightRearTyreState,
    numberOfWheels,
  } = tyrePressureInfoMessage ?? {};
  return (
    <div>
      <div>Id: {id}</div>
      <div>Imei: {imei}</div>
      <div>Created On: {createdOn}</div>
      <div>Number of wheels: {numberOfWheels}</div>
      <div>Left front tyre pressure: {leftFrontTyrePressure}</div>
      <div>Left front tyre temperature: {leftFrontTyreTemperature}</div>
      <div>Left front tyre state: {leftFrontTyreState}</div>
      <div>Right front tyre pressure: {rightFrontTyrePressure}</div>
      <div>Right front tyre temperature: {rightFrontTyreTemperature}</div>
      <div>Right front tyre state: {rightFrontTyreState}</div>
      <div>Left rear tyre pressure: {leftRearTyrePressure}</div>
      <div>Left rear tyre temperature: {leftRearTyreTemperature}</div>
      <div>Left rear tyre state: {leftRearTyreState}</div>
      <div>Right front tyre pressure: {rightRearTyrePressure}</div>
      <div>Right front tyre temperature: {rightRearTyreTemperature}</div>
      <div>Right front tyre state: {rightRearTyreState}</div>
    </div>
  );
}
